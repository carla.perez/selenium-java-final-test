package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProductPage {
    private static WebDriver driver;

    ProductPage(WebDriver driver){
        ProductPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "h1")
    private WebElement pageTitle;

    @FindBy(className = "price")
    private WebElement price;

    @FindBy(className = "add-to-cart-buttons")
    private WebElement cartButton;

    @FindBy(id = "attribute92")
    private WebElement colorSelector;


    @FindBy(id = "attribute180")
    private WebElement sizeSelector;


    @Step("Obteniendo precio del producto")
    public String getPrice(){
        return price.getText();
    }

    @Step("Obteniendo el titulo del producto")
    public String getPageTitle(){
        return pageTitle.getText();
    }


    public void clickColorSelector(){
        colorSelector.click();
    }

    @Step("Haciendo clic en la opción del color")
    public void selectColor(String option){


        Select dropDownElement = new Select(colorSelector);
        dropDownElement.selectByVisibleText(option);

    }


    public void clickSizeSelector(){
        sizeSelector.click();
    }

    @Step("Haciendo clic en la opcion de medida")
    public void selectSize(String option){

        Select dropDownElement = new Select(sizeSelector);
        dropDownElement.selectByVisibleText(option);
    }



    @Step("Agregando producto al carrito")
    public DashboardPage clickCartButton(){
        cartButton.click();
        return new DashboardPage(driver);
    }


}
