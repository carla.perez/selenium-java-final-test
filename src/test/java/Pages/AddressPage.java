package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AddressPage {
    private static WebDriver driver;

    AddressPage(WebDriver driver){
        AddressPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(tagName = "h1")
    private WebElement titlePage;

    @FindBy(id = "telephone")
    private WebElement inputTelephone;

    @FindBy(id = "street_1")
    private WebElement inputStreet;

    @FindBy(id = "city")
    private WebElement inputCity;

    @FindBy(id = "zip")
    private WebElement inputZip;

    @FindBy(id = "country")
    private WebElement countrySelect;

    @FindBy(xpath = "//*[@id=\"form-validate\"]/div[3]/button/span/span")
    private WebElement saveButton;




    public void selectCountry(String option){

        Select dropDownElement = new Select(countrySelect);
        dropDownElement.selectByVisibleText(option);

    }

    public void addNewAddress(String tel, String street, String city, String zip, String option){
        inputTelephone.sendKeys(tel);
        inputStreet.sendKeys(street);
        inputCity.sendKeys(city);
        inputZip.sendKeys(zip);
        selectCountry(option);
    }

    public AddressBookPage clickSaveButton(){
        saveButton.click();
        return new AddressBookPage(driver);
    }



}
