package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import java.util.Random;

public class CreateAccount {
    private static WebDriver driver;

    public CreateAccount(WebDriver driver){
        CreateAccount.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(tagName = "h1")
    private WebElement titleCreateAccountPage;

    @FindBy(id = "firstname")
    private WebElement inputFirstName;

    @FindBy(id = "lastname")
    private WebElement inputLastName;

    @FindBy(id = "email_address")
    private WebElement inputEmailAddress;

    @FindBy(id = "password")
    private WebElement inputPassword;

    @FindBy(id = "confirmation")
    private WebElement inputConfirmPassword;

    @FindBy(xpath = "//*[@id=\"form-validate\"]/div[2]/button")
    private WebElement buttonRegister;

    Random rd = new Random();
    Integer nro = rd.nextInt(100) + 1;

    public void registerAccount(String firstName, String lastName, String password, String confirmPassword){

        inputFirstName.sendKeys(firstName);
        inputLastName.sendKeys(lastName);
        inputEmailAddress.sendKeys(firstName+lastName+nro+"@gmail.com");
        inputPassword.sendKeys(password);
        inputConfirmPassword.sendKeys(confirmPassword);



    }

    public DashboardPage clickRegisterButton(){
        buttonRegister.click();
        return new DashboardPage(driver);
    }

    public String getTitleCreateAccountPage(){
        return titleCreateAccountPage.getText();
    }
}
