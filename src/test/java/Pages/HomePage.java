package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private static WebDriver driver;

    public HomePage(WebDriver driver){
        HomePage.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy (id = "search")
    private WebElement searchBar;

    @FindBy(xpath = "//*[@id=\"header\"]/div/div[2]/a[3]")
    private WebElement account;

    @FindBy(linkText = "My Account")
    private WebElement myAccount;

    public ProductSearchPage search(String product){
        searchBar.clear();
        searchBar.sendKeys(product);
        searchBar.sendKeys(Keys.ENTER);
        return new ProductSearchPage(driver);
    }

    public void clickAccount(){
        account.click();

    }
    public AccountPage clickMyAccount(){
        myAccount.click();
        return new AccountPage(driver);
    }

}
