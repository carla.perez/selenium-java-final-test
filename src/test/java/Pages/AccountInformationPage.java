package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountInformationPage {
    private static WebDriver driver;

    AccountInformationPage(WebDriver driver){
        AccountInformationPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(tagName = "h2")
    private WebElement title;

    @FindBy(className = "checkbox")
    private WebElement checkbox;

    @FindBy(name = "current_password")
    private WebElement inputCurrentPassword;

    @FindBy(name = "password")
    private WebElement inputNewPassword;

    @FindBy(name = "confirmation")
    private WebElement inputConfirmPassword;

    @FindBy(xpath = "//*[@id=\"form-validate\"]/div[3]/button")
    private WebElement saveButton;

    public void changePassword(String currentPass, String newPass, String confirmPass){
        inputCurrentPassword.sendKeys(currentPass);
        inputNewPassword.sendKeys(newPass);
        inputConfirmPassword.sendKeys(confirmPass);
    }

    public DashboardPage clickSaveButton(){
        saveButton.click();
        return new DashboardPage(driver);
    }

    public String getTitlePage(){
        return title.getText();
    }

    public Boolean checkboxValidation(){
        return checkbox.isSelected();
    }
}
