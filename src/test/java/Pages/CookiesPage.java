package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CookiesPage {

    private static WebDriver driver;

    public CookiesPage(WebDriver driver){
        CookiesPage.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "success-msg")
    private WebElement noticeMessage;

    public String getNoticeMessage(){
        return noticeMessage.getText();
    }
}
