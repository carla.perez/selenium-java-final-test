package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountPage {
    private static WebDriver driver;

    public AccountPage(WebDriver driver){
        AccountPage.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(tagName = "h1")
    private WebElement titleAccountPage;

    @FindBy(className = "buttons-set")
    private WebElement buttonCreateAccount;

    @FindBy(id = "email")
    private WebElement inputEmail;

    @FindBy(id = "pass")
    private WebElement inputPass;

    @FindBy(id = "send2")
    private WebElement loginButton;

    @Step("Obteniendo el titulo de la pagina")
    public String getTitlePageAccount(){
        return titleAccountPage.getText();
    }

    @Step("Creando cuenta")
    public CreateAccount clickButtonCreateAccount(){
        buttonCreateAccount.click();
        return new CreateAccount(driver);
    }
    @Step("Completando email y password")
    public void login(String email, String password){
        inputEmail.sendKeys(email);
        inputPass.sendKeys(password);

    }

    public DashboardPage clickLogin(){
        loginButton.click();
        return new DashboardPage(driver);
    }
}
