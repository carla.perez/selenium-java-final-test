package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddressBookPage {
    private static WebDriver driver;

    AddressBookPage(WebDriver driver){
        AddressBookPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "success-msg")
    private WebElement successMessage;


    @FindBy(xpath = "/html/body/div/div[2]/div[2]/div/div[2]/div/div[1]/button")
    private WebElement addNewAddressButton;

    public String getSuccessMsg(){
       return successMessage.getText();
    }

    public AddressPage clickAddNewAddressButton(){

        addNewAddressButton.click();
        return new AddressPage(driver);
    }
}
