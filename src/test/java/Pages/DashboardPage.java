package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {
    private static WebDriver driver;

    public DashboardPage(WebDriver driver){
        DashboardPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "success-msg")
    private WebElement successMessage;

    @FindBy(className = "welcome-msg")
    private WebElement welcomeMessage;

    @FindBy(className = "hello")
    private WebElement helloMessage;

    @FindBy(linkText = "Change Password")
    private WebElement changePassword;

    @FindBy(tagName = "h1")
    private WebElement title;


    @FindBy(xpath = "//*[@id=\"top\"]/body/div/div[2]/div[2]/div/div[2]/div/div/div[4]/div[1]/a")
    private WebElement manageAddress;

    public  String getSuccessMessage(){
        return successMessage.getText();
    }

    public  String getWelcomeMessage(){
        return welcomeMessage.getText();
    }

    public String getHelloMessage(){
        return helloMessage.getText();
    }

    public String getTitle(){
        return title.getText();
    }

    public AccountInformationPage clickChangePassword(){
        changePassword.click();
        return new AccountInformationPage(driver);
    }

    public AddressBookPage clickManageAddress(){
        manageAddress.click();
        return new AddressBookPage(driver);
    }


}
