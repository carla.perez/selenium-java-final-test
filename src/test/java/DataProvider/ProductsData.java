package DataProvider;

import org.testng.annotations.DataProvider;

public class ProductsData {
    @DataProvider(name = "products")
    public static Object[][] getProductsData() {
        return new Object[][]{
                {"TRIBECA SKINNY JEAN", "$185.00", "Black", "31", "TriBeCa Skinny Jean was added to your shopping cart."},
                {"STRETCH COTTON BLAZER", "$490.00", "Blue", "L", "Stretch Cotton Blazer was added to your shopping cart."},

        };
    }
}
