package DataProvider;

import org.testng.annotations.DataProvider;

public class AccountData {
    @DataProvider(name = "accounts")
    public static Object[][] getAccountsData() {
        return new Object[][]{
                {"Juan", "Perez", "juanPerez22@gmail.com", "123456", "123456"},


        };
    }
    @DataProvider(name = "accountLogin")
    public static Object[][] getAccountLoginData() {
        return new Object[][]{
                {"juanPerez20@gmail.com", "123456", "Hello, Juan Perez!"},


        };
    }
}
