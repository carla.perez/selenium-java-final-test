package Scritps;

import Pages.*;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AddressTests extends BaseTests{
    @Parameters({"emailParam", "passParam", "msgHelloParam", "telParam", "streetParam", "cityParam", "zipParam", "optionParam", "msgSuccessParam"})
    @Test
    public void testChangeAddress(String emailAddress, String password, String helloMsg, String tel, String street, String city, String zip, String option, String successMsg){

        homePage.clickAccount();
        AccountPage accountPage = homePage.clickMyAccount();
        assertEquals(accountPage.getTitlePageAccount(), "LOGIN OR CREATE AN ACCOUNT");
        accountPage.login(emailAddress, password);
        DashboardPage dashboardPage = accountPage.clickLogin();
        assertEquals(dashboardPage.getHelloMessage(), helloMsg);
        AddressBookPage addressBookPage = dashboardPage.clickManageAddress();
        AddressPage addressPage = addressBookPage.clickAddNewAddressButton();
        addressPage.addNewAddress(tel, street, city, zip, option);
        addressBookPage = addressPage.clickSaveButton();
        assertEquals(addressBookPage.getSuccessMsg(), successMsg);



    }
}
