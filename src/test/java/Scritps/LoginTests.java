package Scritps;

import Pages.AccountPage;
import Pages.DashboardPage;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LoginTests extends BaseTests {
    @Parameters({"emailParam", "passParam", "msgParam"})
    @Test
    public void testLogin(String emailAddress, String password, String message) {

        homePage.clickAccount();
        AccountPage accountPage = homePage.clickMyAccount();
        assertEquals(accountPage.getTitlePageAccount(), "LOGIN OR CREATE AN ACCOUNT");
        accountPage.login(emailAddress, password);
        DashboardPage dashboardPage = accountPage.clickLogin();
        assertEquals(dashboardPage.getWelcomeMessage(), message);
    }
}
