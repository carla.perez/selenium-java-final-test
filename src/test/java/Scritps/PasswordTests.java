package Scritps;

import Pages.AccountInformationPage;
import Pages.AccountPage;
import Pages.DashboardPage;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PasswordTests extends BaseTests{
    @Parameters({"emailParam", "passParam", "msgHelloParam", "currentPassParam", "newPassParam", "confirmPassParam", "msgSuccessParam"})
    @Test
    public void testChangePassword(String emailAddress, String password, String helloMsg, String currentPass, String newPass, String confirmPass, String successMsg){

        homePage.clickAccount();
        AccountPage accountPage = homePage.clickMyAccount();
        assertEquals(accountPage.getTitlePageAccount(), "LOGIN OR CREATE AN ACCOUNT");
        accountPage.login(emailAddress, password);
        DashboardPage dashboardPage = accountPage.clickLogin();
        assertEquals(dashboardPage.getTitle(), helloMsg);
        AccountInformationPage accountInformationPage = dashboardPage.clickChangePassword();
        assertTrue(accountInformationPage.checkboxValidation(), "true");
        accountInformationPage.changePassword(currentPass, newPass, confirmPass);
        dashboardPage = accountInformationPage.clickSaveButton();
        assertEquals(dashboardPage.getSuccessMessage(), successMsg);
        assertEquals(dashboardPage.getTitle(),"MY DASHBOARD");

        accountInformationPage = dashboardPage.clickChangePassword();
        accountInformationPage.changePassword(newPass, currentPass, currentPass);
        accountInformationPage.clickSaveButton();

    }
}
