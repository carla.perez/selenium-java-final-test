package Scritps;

import Pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class BaseTests {

    private static WebDriver driver;
    protected HomePage homePage;

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://magento-demo.lexiconn.com/");

        homePage = new HomePage(driver);

    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }

}
