package Scritps;

import DataProvider.ProductsData;
import Pages.DashboardPage;
import Pages.ProductPage;
import Pages.ProductSearchPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CartTests extends BaseTests{
    @Test(dataProvider = "products", dataProviderClass = ProductsData.class)
    public void testSearch(String product, String price, String color, String size, String message) throws Exception {

        ProductSearchPage productSearchPage = homePage.search(product);
        ProductPage productPage = productSearchPage.clickProduct(product);

        assertEquals(productPage.getPageTitle(), product);
        productPage.selectColor(color);
        productPage.selectSize(size);
        DashboardPage dashboardPage = productPage.clickCartButton();
        assertEquals(dashboardPage.getSuccessMessage(), message);

    }
}
