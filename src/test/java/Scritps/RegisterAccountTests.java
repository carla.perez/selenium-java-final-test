package Scritps;

import Pages.AccountPage;

import Pages.CreateAccount;
import Pages.DashboardPage;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class RegisterAccountTests extends BaseTests{
    @Parameters({"firstNameParam", "lastNameParam", "passParam", "confirmPassParam", "msgParam"})
    @Test
    public void testAccount(String firstName, String lastName, String password, String confirmPassword, String message) throws Exception{

        homePage.clickAccount();
        AccountPage accountPage = homePage.clickMyAccount();
        assertEquals(accountPage.getTitlePageAccount(), "LOGIN OR CREATE AN ACCOUNT");
        CreateAccount createAccount = accountPage.clickButtonCreateAccount();
        assertEquals(createAccount.getTitleCreateAccountPage(), "CREATE AN ACCOUNT");
        createAccount.registerAccount(firstName, lastName, password, confirmPassword);
        DashboardPage dashboardPage = createAccount.clickRegisterButton();
        assertEquals(dashboardPage.getSuccessMessage(), message);


    }
}
