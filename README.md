# Selenium Java Final Test

## Description
Selenium Java final test for the Abstracta Academy course.
This project contains 5 test cases designed for the site [Madison Island](http://magento-demo.lexiconn.com/) using Selenium WebDriver.
Page Object Pattern was implemented for the web elements. 
Data Driven Testing was performed using TestNG framework.
Allure reports are included.

## Visuals
[project](img/project.png)
[allure](img/allure-report.png)

## Installation
* Install [IntelliJ IDEA](https://www.jetbrains.com/idea/download/#section=windows)
* Install [JDK8](https://www.oracle.com/java/technologies/downloads/#JDK8)
* Install [Maven](https://maven.apache.org/download.cgi)
* Install [Allure](https://qameta.io/allure-report/)

## Executing IntelliJ
* Execute "mvn compile"
* Execute "allure serve allure-results" to open the report after executing the tests  

## Executing Tests or Suites
* Execute "mvn test"
* Execute "mvn clean" to delete files from the Target folder 

## testng.xml
* For running the complete test suite, right click on the testng.xml file.
